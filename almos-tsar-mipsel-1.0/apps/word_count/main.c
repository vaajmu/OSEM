#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#define PAGE_SIZE (4096)
/* #define NB_WORKERS (4) */
/* #define NB_PAGES 64 */
/* #define NB_CHAR (4096*NB_PAGES) */

#ifndef NB_WORKERS
#define NB_WORKERS (4)
#endif

#ifndef NB_PAGES
#define NB_PAGES (64)
#endif

#ifndef NB_CHAR
#define NB_CHAR (PAGE_SIZE * NB_PAGES)
#endif



int* accessWordlen(int i, int j);
void*getAllignedPage(int nbPages);
void generate();
void* threadWork(void* ptr);

char*doc;
char *wordLen;
clock_t debThread[NB_WORKERS];
clock_t finThread[NB_WORKERS];
double totalTimeThread[NB_WORKERS];
int debug[32+1];

void generate(){
	int i, myRand,jDebug;
	
	memset(debug,0,32);

	i=0;
	while(i < NB_CHAR)
	{

		myRand =(rand()%31) + 1;
		debug[myRand]++;		

		if((i+myRand) >= NB_CHAR)
		{
			debug[myRand]--;
			
			myRand = NB_CHAR-i-1;
			debug[myRand]++;
			if(myRand==0) 
			{
				if(doc[i-2] == ' '){
					doc[i-1] = '-';
					doc[i] = '-';
				}
				else{
					doc[i-1] = ' ';
					doc[i] = '-';
				}
				
				return;
			}
		}

		i+=myRand;
		while(myRand>0){
			doc[i-myRand] = '-';
			myRand--;
		}	

		doc[i] = ' ';
		i++;
	}
}

void* threadWork(void* ptr)
{

	long clk_tck = CLOCKS_PER_SEC;
	clock_t t1, t2;
	int cmpt;
	int myId = (int)ptr;
	int i,j;//debug
        int *results = NULL;

        results = accessWordlen(myId,0);

	debThread[myId] = clock();
	t1 = clock();

	memset(results, 0, (32+2)*sizeof(int));
        
	i=myId*(NB_CHAR/NB_WORKERS);
	cmpt=0;

	for(;doc[i] != ' '; i++)cmpt++;

	i++;
	results[0] = cmpt;
        cmpt=0;


	for(;i< (myId+1)*(NB_CHAR/NB_WORKERS); i++){
		if(doc[i]==' ') {
			results[cmpt]++;
			cmpt=0;
		} 
		else {
			cmpt++;
		}
	}
	results[33] = cmpt;
	
	t2 = clock();
	finThread[myId] = t2;
	
	/*printf("[ID=%d] Nb ticks/seconde = %ld,  Nb ticks depart : %ld, Nb ticks final : %ld\n", myId, clk_tck, (long)t1, (long)t2);
	printf("[ID=%d] Temps consomme (s) : %lf \n", myId, (double)(t2-t1)/(double)clk_tck);*/
	totalTimeThread[myId] = (double)(t2-t1)/(double)clk_tck;

	return NULL;
}

void*getAllignedPage(int nbPages){

	char*ptr;

        if( (ptr = (char*)malloc((nbPages+1) * PAGE_SIZE * sizeof(char))) == ((void*)0) )
                exit(EXIT_FAILURE);

        if(((int)ptr)%PAGE_SIZE)
                ptr += (PAGE_SIZE-(((int)ptr)%PAGE_SIZE));

        return ptr;
}

int* accessWordlen(int i, int j) {
	return ((int*)(wordLen+(i*PAGE_SIZE*sizeof(char))))+j;
}

int main()
{
	int i,j;
	pthread_t workers[NB_WORKERS];
	long clk_tck = CLOCKS_PER_SEC;
	clock_t debMerge, finMerge, lanceThreads, premThreadExec, dernThreadExec, debProg;

	//TODO allocation avant ou après clock ? Fait partie du problem
	doc = (char*)getAllignedPage(NB_PAGES);
	wordLen = (char*)getAllignedPage(NB_WORKERS);

	debProg=clock();
	srand(getpid());
	generate();
	
	lanceThreads = clock();
	#if NB_WORKERS != 1
	for(i=0; i<NB_WORKERS; i++){
			pthread_create(workers+i, NULL, threadWork, (void*)i);
	}

	for(i=0; i<NB_WORKERS; i++){
		pthread_join(workers[i], NULL);
	}
	#else
		threadWork((void*)0);
	#endif

	debMerge = clock();
	
	(*accessWordlen(0,*accessWordlen(0,0)))++;//deb prem thread
	(*accessWordlen(0,*accessWordlen(NB_WORKERS-1,33)))++;//fin dern thread

	for(i=0;i<(NB_WORKERS-1); i++)
		(*accessWordlen(0,*accessWordlen(i,33) + *accessWordlen(i+1,0)))++;
	
	finMerge = clock();
	
	premThreadExec = clock();
	for(i=0;i<NB_WORKERS;i++)
		if(debThread[i]<premThreadExec)
			premThreadExec = debThread[i];
	
	dernThreadExec = (clock_t)0;
	for(i=0;i<NB_WORKERS;i++)
		if(finThread[i]>dernThreadExec)
			dernThreadExec = finThread[i];
	
	printf("\n\t[Main]\n");
        printf("%d workers, %d carac, %d pages, 0 padding\n", NB_WORKERS, NB_CHAR, NB_CHAR/4096);
	printf("lanceThreads=%ld\n debMerge=%ld\n finMerge=%ld\n",(long int)lanceThreads, (long int)debMerge, (long int)finMerge);
	printf("premThreadExec=%ld\n dernThreadExec=%ld\n",(long int)premThreadExec, (long int)dernThreadExec);
	printf("Const %f\n",(double)CLOCKS_PER_SEC);
	printf("Nombre de pages %d\n", (NB_CHAR)/4096);
	printf("[Main] Fin ");
/*
	printf("\n\n-----\n\nLen\tOcurr\tDeb\tOK?\n");
	for(j=1;j<(32+1); j++)
	{
		for(i=1;i<NB_WORKERS;i++)
		{
			wordLen[0][j]+=wordLen[i][j];
		}
		printf("%d\t%d\t%d\t%d\n",j,wordLen[0][j],debug[j],debug[j]==wordLen[0][j]);
	}
*/
	exit(EXIT_SUCCESS);
}
