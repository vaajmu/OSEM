#! /bin/bash

usage()
{
    echo "Usage: $0 path_to_hdd {XX | OMP_THREADS_NR} bin_name [arg1 arg2 arg3 ...]"
    exit 1
}

mkshrc()
{
    th=$1; shift
    cmd=$*
        
    echo "echo stared"

    if [ $th != "XX" ]; then
	echo "export OMP_NUM_THREADS $th"
    fi

    echo "echo exec /bin/$cmd"
    echo "exec /bin/$cmd"
    echo "echo done"
}


if [ $# -lt 3 ]; then
    echo "ERROR: missing arguments"
    usage
fi

tmpfile=/tmp/shrc$$

hdd=$1; shift

mkshrc $* > $tmpfile

mcopy -i $hdd $tmpfile ::etc/shrc

rm -f $tmpfile
